import {Gender} from "./Gender";

export interface Student {
  firstName: string;
  lastName: string;
  gender: string;
  skillScore: number;
}
