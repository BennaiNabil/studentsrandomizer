import {Component} from '@angular/core';
import {Student} from "../models/Student";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'groupcreation';

  studentList: Student[] = [
    {"firstName": "Sanchez", "lastName": "Benton", "gender": "male", "skillScore": 35},
    {"firstName": "Lauren", "lastName": "Hunt", "gender": "female", "skillScore": 86},
    {"firstName": "Pitts", "lastName": "Kane", "gender": "male", "skillScore": 52},
    {"firstName": "Janell", "lastName": "Head", "gender": "female", "skillScore": 29},
    {"firstName": "Valencia", "lastName": "Romero", "gender": "male", "skillScore": 95},
    {"firstName": "Gabrielle", "lastName": "Stanley", "gender": "female", "skillScore": 76},
    {"firstName": "Alejandra", "lastName": "Hoffman", "gender": "female", "skillScore": 37},
    {"firstName": "Deann", "lastName": "Floyd", "gender": "female", "skillScore": 30},
    {"firstName": "Margery", "lastName": "Rollins", "gender": "female", "skillScore": 74},
    {"firstName": "Shaffer", "lastName": "Atkinson", "gender": "male", "skillScore": 66},
    {"firstName": "Reyna", "lastName": "Martinez", "gender": "female", "skillScore": 84},
    {"firstName": "Britney", "lastName": "Woods", "gender": "female", "skillScore": 81}
  ];


  addStudent(newStudent: Student) {
    this.studentList.push(newStudent);
  }

}
