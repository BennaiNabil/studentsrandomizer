import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Student} from "../../models/Student";
import {RandomGroupsUtils} from "../../utils/RandomGroupsUtils";

@Component({
  selector: 'app-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.css']
})
export class StudentTableComponent implements OnInit, OnChanges {
  @Input() studentList: Student[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.studentList);
  }

  shuffleStudents() {
    this.studentList = RandomGroupsUtils.shuffleArray(this.studentList);
  }

  sortStudents(field: string, order: string) {
    switch (field) {
      case 'firstname':
        this.studentList = this.studentList.sort((a, b) => {
          if (a.firstName < b.firstName) return order === 'asc' ? -1 : 1;
          if (a.firstName > b.firstName) return order === 'asc' ? 1 : -1;
          return 0;
        });
        break;
      case 'lastname':
        this.studentList = this.studentList.sort((a, b) => {
          if (a.lastName < b.lastName) return order === 'asc' ? -1 : 1;
          if (a.lastName > b.lastName) return order === 'asc' ? 1 : -1;
          return 0;
        });
        break;
      case 'gender':
        this.studentList = this.studentList.sort((a, b) => {
          if (a.gender < b.gender) return order === 'asc' ? -1 : 1;
          if (a.gender > b.gender) return order === 'asc' ? 1 : -1;
          return 0;
        });
        break;
      case 'skillscore':
        this.studentList = this.studentList.sort((a, b) => {
          if (a.skillScore < b.skillScore) return order === 'asc' ? -1 : 1;
          if (a.skillScore > b.skillScore) return order === 'asc' ? 1 : -1;
          return 0;
        });
        break;
      default:
        break;
    }

  }
}
