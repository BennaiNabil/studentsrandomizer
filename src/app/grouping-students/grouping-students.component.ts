import { Component, Input, OnInit } from '@angular/core';
import { Student } from "../../models/Student";
import { RandomGroupsUtils } from "../../utils/RandomGroupsUtils";

@Component({
  selector: 'app-grouping-students',
  templateUrl: './grouping-students.component.html',
  styleUrls: ['./grouping-students.component.css']
})
export class GroupingStudentsComponent implements OnInit {
  @Input() studentList: Student[] = [];
  nbPerGroup: number = 3;
  groups: Student[][] = [[]];
  areGroupsFormed: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  submitForm() {
    if (this.nbPerGroup != null) {
      this.groups = RandomGroupsUtils.createRandomPartitions(this.studentList, this.nbPerGroup);
      this.areGroupsFormed = true;
    }
  }
}
