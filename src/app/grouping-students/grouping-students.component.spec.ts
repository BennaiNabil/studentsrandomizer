import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupingStudentsComponent } from './grouping-students.component';

describe('GroupingStudentsComponent', () => {
  let component: GroupingStudentsComponent;
  let fixture: ComponentFixture<GroupingStudentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupingStudentsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GroupingStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
