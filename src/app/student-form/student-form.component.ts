import {Component, OnInit} from '@angular/core';
import {Student} from "../../models/Student";
import {Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Output() newStudentEvent = new EventEmitter<Student>();

  genderList: string[] = ["Male", "Female"];
  firstNameInput: string = "";
  lastNameInput: string = "";
  genderInput: string = "";
  skillScoreInput: number = 0;
  studentToEmit: Student | undefined;

  constructor() {
  }

  ngOnInit(): void {
  }


  submitForm() {
    this.studentToEmit = {
      'firstName': this.firstNameInput,
      'lastName': this.lastNameInput,
      'gender': this.genderInput,
      'skillScore': this.skillScoreInput
    };
    this.newStudentEvent.emit(this.studentToEmit);

  }
}
