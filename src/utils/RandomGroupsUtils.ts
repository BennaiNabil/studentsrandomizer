import {Student} from "../models/Student";


export class RandomGroupsUtils {
  /**
   * Shuffles an array without mutating the one inserted as an argument
   * (Uses Fisher–Yates' algorithm)
   * See more <a href="https://en.wikipedia.org/wiki/Fisher–Yates_shuffle">here</a>
   * @param arrayToShuffle T[]
   */
  static shuffleArray<T>(arrayToShuffle: T[]): T[] {
    // Copying the array with a spread function
    const shuffledArrray: T[] = [...arrayToShuffle];
    // Starting from the end of the array
    let currentIndex = shuffledArrray.length;
    let randomIndex;
    while (currentIndex !== 0) {
      // Choosing a random index
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      // Swapping the elements at indeces randomIndex and currentIndex
      [shuffledArrray[currentIndex], shuffledArrray[randomIndex]] = [shuffledArrray[randomIndex], shuffledArrray[currentIndex]];
    }
    return shuffledArrray;
  }

  /**
   * Partitions an array into fixed sized chunks without changing the order, leaving the remainder in the last array
   * @param arrayToPartition
   * @param partitionSize
   */
  static partitionArray<T>(arrayToPartition: T[], partitionSize: number): T[][] {
    const partitions: T[][] = [];
    // We cannot build a group of N elements from M elements if M > N
    if (partitionSize > arrayToPartition.length) {
      throw new Error("Not enough elements in the array to create partitions this size");
    }
    // Cuting every partitionSize elements in the array, leaving the remainder
    for (let i: number = 0; i < arrayToPartition.length; i += partitionSize) {
      partitions.push(arrayToPartition.slice(i, i + partitionSize));
    }
    return partitions;
  }

  /**
   * Stiches two arrays together
   * <p> Example: [1,2,3] and [10,11,12] stiched together gives [1,10,2,11,3,12]</p>
   * @param leftArray
   * @param rightArray
   */
  static stitchTwoArrays<T>(leftArray: T[], rightArray: T[]): T[] {
    const arr = [];
    const length = Math.max(leftArray.length, rightArray.length);
    for (let i: number = 0; i < length; i++) {
      // Taking the first element from each array then taking the second of each etc...
      // Compact way of writing the conditions handling the different sizes of the arrays
      i < leftArray.length && arr.push(leftArray[i]);
      i < rightArray.length && arr.push(rightArray[i]);
    }
    return arr;
  }

  /**
   * Partitions randomly an array into fixed sized chunks, leaving the remainder in the last array
   * @param arrayToPartition
   * @param partitionSize
   */
  static createRandomPartitions<T>(arrayToPartition: T[], partitionSize: number): T[][] {
    // A random partition is a partition of a shuffled array
    return this.partitionArray(this.shuffleArray(arrayToPartition), partitionSize);
  }


  /**
   * Partitions an array into fixed sized chunks, with a certain gender balance
   *
   * @param arrayToPartition
   * @param partitionSize
   */
  static createPartitionWithGenderBalance(arrayToPartition: Student[], partitionSize: number): Student[][] {
    // split the array into a male and female array and shuffle them
    const shuffledMales: Student[] = this.shuffleArray(arrayToPartition.filter(el => el.gender === "male"));
    const shuffledFemales: Student[] = this.shuffleArray(arrayToPartition.filter(el => el.gender === "female"));
    // stiches the arrays to evenly distribute them
    const stichedStudents: Student[] = this.stitchTwoArrays(shuffledMales, shuffledFemales);
    // partition the equally distributed array
    return this.partitionArray(stichedStudents, partitionSize);
  }


  /**
   * Partitions an array into fixed sized chunks, with a certain skill level balance
   *
   * @param arrayToPartition
   * @param partitionSize
   */
  static createPartitionWithSkillBalance(arrayToPartition: Student[], partitionSize: number): Student[][] {
    const sortedBySkill: Student[] = arrayToPartition.sort((a, b) => a.skillScore - b.skillScore);
    const pluckedArr: Student[] = [];
    for (let i: number = 0; i < sortedBySkill.length / 2; i++) {
      // Taking the first element of the array, then the last element, then the second element, then the second to last element, etc.
      pluckedArr.push(sortedBySkill[i]);
      pluckedArr.push(sortedBySkill[(sortedBySkill.length - 1) - i]);
    }
    return this.partitionArray(pluckedArr, partitionSize);
  }
}
